<?php

class Data{
    public $host       = 'localhost';
    public $database   = 'test';
    public $user       = 'root';
    public $pass       = '';
    public $flag       = false;
    public $result;
    public $conn;
    private $data;

    public function __construct($data = array()){

        $mysqli = new mysqli($this->host, $this->user, $this->pass, $this->database);
        if($mysqli->connect_errno){
            die("Connection failed: " . $mysqli->connect_error);
        }else  {
            $this->flag = true;
            $this->conn = $mysqli;
        }

        //create data table if it not exists
        if(is_array($data)){
            $table_name = get_class($this);
            $sql = "CREATE TABLE IF NOT EXISTS " . strtolower ($table_name) . " ( `ID` int NOT NULL AUTO_INCREMENT ,";

            foreach($data as $name => $type){
                $sql .= "`{$name}` {$type} NULL,";
                $this->data[] = $name;
            }

            $sql .= "`LastEdited` datetime DEFAULT NULL, `Created` datetime DEFAULT NULL, deleted boolean NULL DEFAULT false, PRIMARY KEY (`ID`)) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1" ;

            $this->result = $mysqli->query($sql);
            if(!$this->result){
                die("query failed: " . $mysqli->connect_error);
            }
        }
    }

    public function insert($data = array()){
        $table = get_class($this);

        $string_title = '';
        $string_value = '';
        $last = count($data);
        $c = 1;

        foreach($data as $title => $value){
            if($c == $last){
                $string_title .= "{$title}";
                $string_value .= is_numeric($value)? "{$value}" : "'{$value}'";
            }else{
                $string_title .= "{$title}, ";
                $string_value .= is_numeric($value)? "{$value}, " : "'{$value}', ";
            }
            $c++;
        }
//        $date = date('Y-m-d H:i:s', time());
        $sql = "INSERT INTO {$table} ({$string_title}, LastEdited, Created) VALUES ({$string_value},now(),now())";
//        var_dump($sql);die;

        $this->result = mysqli_query($this->conn, $sql);
        if(!$this->result){
            die("query failed: " . mysqli_error($this->conn));
        }
    }

    public function update($id ,$data = array()){
        $table = get_class($this);

        $string = '';
        $last = count($data);
        $c = 1;

        foreach($data as $title => $value){
            if($c == $last){
                $string .= is_numeric($value)? "{$title} = {$value}" : "{$title} = '{$value}'";
            }else{
                $string .= is_numeric($value)? "{$title} = {$value}, " : "{$title} = '{$value}', ";
            }
            $c++;
        }

//        $date = date('Y-m-d H:i:s', time());
        $sql = "UPDATE {$table} SET {$string} , LastEdited = now() WHERE id = {$id}";
//        var_dump($sql);die;

        $this->result = mysqli_query($this->conn, $sql);
        if(!$this->result){
            die("query failed: " . mysqli_error($this->conn));
        }
    }

    public function delete($id){
        $table = get_class($this);
        $sql = "DELETE FROM {$table} WHERE id = " . $id;

        $this->result = mysqli_query($this->conn, $sql);
        if(!$this->result){
            die("query failed: " . mysqli_error($this->conn));
        }
    }

    public function __get($data = array()){
        $table = get_class($this);
        $sql = "SELECT {$data['name']} FROM {$table} WHERE id = " . $data['id'];

        $this->result = mysqli_query($this->conn, $sql);
        if(!$this->result){
            die("query failed: " . mysqli_error($this->conn));
        }
    }

    public function __destruction(){
        if($this->flag){
            if($this->result !== null){
                mysqli_free_result($this->result);
            }
            mysqli_close($this->conn);
        }
    }
}

class table_test extends Data{

}

$data = array();
$data = array(
    'user' => 'varchar(10)',
    'pass' => 'text',
    'email' => 'text',
    'avatar' => 'text'
);
$table = new table_test($data);

$data = array(
    'user' => 'test_2',
    'pass' => '123445swa2',
    'email' => 'test@gmail.com',
);
//$table->insert($data);
//$table->update(1,$data);
//$table->delete(1);
var_dump(date('Y-m-d H:i:s', time()));